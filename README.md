projekt gry RTS
Sterowanie:
- zaznaczanie jednostek: LPM (klikanie pojedyńczych jednostek lub przeciąganie myszy przytrzymując LPM w celu zaznaczenia większej ilości jednostek)
- poruszanie się: WSAD, zbliżanie myszy do krawędzi ekranu
- obrót kamery: Przytrzymanie PPM + poruszanie myszą
- przybliżenie/oddalenie kamery: Koło myszy
- pauza/Szybkie Menu: Escape

Celem gry jest pokonanie wszystkich jednostek wroga. Gra kończy się wtedy, gdy którykolwiek z graczy straci wszystkie swoje jednostki.
By tworzyć jednostki zbierające zasoby należy wybudować Town Hall. Jednostki bojowe tworzy się w Barracks.
By wybrać budynek do zbudowania należy się upewnić, że nie jest zaznaczony żaden obiekt i wybrać budynek z listy w lewym dolnym rogu ekranu.
Po zaznaczeniu budynku w lewym dolnym rogu pojawi się lista jednostek jakie można stworzyć w zaznaczonym budynku.

Tworzenie budynków i jednostek wymaga zasobów. Zasoby zbiera się specjalnymi jednostkami (Workers), zaznaczając jednostki i klikając PPM na zasób (krzak jagód, złoże złota, kamień, drzewo).
